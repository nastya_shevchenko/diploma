package com.minmaxproblem;

import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.Processor;
import com.minmaxproblem.objects.Task;
import com.minmaxproblem.services.BfsPolynomial;
import com.minmaxproblem.services.FirstFreeProcessorAlgoritm;
import com.minmaxproblem.services.ProcessorUtils;
import com.minmaxproblem.services.TaskUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Анастасия on 09.02.2016.
 */
public class TestMain {
    @Test
    public void testGenerate(){
        /*ArrayList<Task> taskList = TaskUtils.generateTaskScope(
                new BigDecimal(1000000.0),
                new BigDecimal(1000.0),
                new BigInteger("10"));

        for(Task task:taskList)
        System.out.println("tasks" + task.getDuration());

        ArrayList<Processor> processorList = ProcessorUtils.generateProcessorScope(
                new BigDecimal(3.0),
                new BigDecimal(2.0),
                new BigInteger("3"));

        processorList = FirstFreeProcessorAlgoritm.startFirstFreeProcessorAlgoritm(processorList, taskList);

        for(Processor pr:processorList)
            System.out.println("processor" + pr.getQueueDuration());

        System.out.println("max Queue" +ProcessorUtils.findMaxQueue(processorList).getQueueDuration());
        System.out.println("min Queue" +ProcessorUtils.findMinQueue(processorList).getQueueDuration());*/

        MatrixVertex2 ffpMatrix = FirstFreeProcessorAlgoritm.ffpAlgoritm(TaskUtils.generateTaskMatrix(BigDecimal.valueOf(100.0),
                BigDecimal.valueOf(10.0), BigInteger.valueOf(5), BigInteger.valueOf(2)));
        System.out.println("FfpMatrix: " + ffpMatrix.getMaxQueueLength());
    }
}
