package com.minmaxproblem.objects;

import com.minmaxproblem.services.ProcessorUtils;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MatrixVertex {


    private int M;             // number of rows
    private int N;             // number of columns
    private Task[][] data;   // M-by-N array
    private int level;
    public boolean wasVisited;

    // create M-by-N matrix of 0's
    public MatrixVertex(int M, int N) {//taskAmount* taskAmount
        this.M = M;
        this.N = N;
        data = new Task[M][N];
        this.wasVisited = false;
    }

    // create matrix based on 2d array
    public MatrixVertex(Task[][] data) {
        M = data.length;
        N = data[0].length;
        this.data = new Task[M][N];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                this.data[i][j] = data[i][j];
    }

    // copy constructor
    private MatrixVertex(MatrixVertex A) {
        this(A.data);
    }

    // return C = A + B
    public MatrixVertex plus(MatrixVertex B) {
        MatrixVertex A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixVertex C = new MatrixVertex(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++) {
                C.data[i][j].setDuration(A.data[i][j].getDuration().add(B.data[i][j].getDuration()));
            }
        return C;
    }

    // return C = A + B
    public MatrixVertex addRow(Task[] taskRow, int row) {
        MatrixVertex A = this;
        MatrixVertex C = new MatrixVertex(M, N);
        for (int j = 0; j < N; j++) {
            C.data[row][j] = taskRow[j];
        }
        return C;
    }


    // print matrix to standard output
    public void show() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++){
                System.out.print(data[i][j]!=null?data[i][j].getDuration(): BigDecimal.ZERO +" ");
            }
            System.out.println();
        }
    }
    public BigDecimal getLength(Task[] row) {
        BigDecimal sum = new BigDecimal("0");
        for (int j = 0; j < row.length; j++) {
            if (row[j]!=null) {
                sum=sum.add(row[j].getDuration());
                //System.out.println( " sum " + sum);
            }

        }

        return sum;
    }
    //getMinMaxQueue
    public BigDecimal getMaxQueueLength() {
        BigDecimal maxLength = getLength(data[0]);
        BigDecimal currentLength;
        for (int i = 0; i < M; i++) {
            currentLength = getLength(data[i]);
            if (currentLength.compareTo(maxLength)>0){
                maxLength = currentLength;
            }
            System.out.println();
        }

        return maxLength;
    }
    //get MinQueue from MatrixVertex
    public int getMinQueue() {
        int maxLengthQueue = 0;
        BigDecimal maxLength = getLength(this.data[0]);
        BigDecimal currentLength;
        for (int i = 0; i < M; i++) {
            currentLength = getLength(this.data[i]);
            if (currentLength.compareTo(maxLength)<0){
                maxLength = currentLength;
                maxLengthQueue = i;
            }
            //System.out.println(maxLength + " maxLength "+ maxLengthQueue +" maxLengthQueue" );
        }

        return maxLengthQueue;
    }
    //get MaxQueue from MatrixVertex
    public int getMaxQueue() {
        int maxLengthQueue = 0;
        BigDecimal maxLength = getLength(data[0]);
        BigDecimal currentLength;
        for (int i = 0; i < M; i++) {
            currentLength = getLength(data[i]);
            if (currentLength.compareTo(maxLength)>0){
                maxLength = currentLength;
                maxLengthQueue = i;
            }
            System.out.println();
        }

        return maxLengthQueue;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++){
                if (data[i][j]!=null)
                hash = 89 * data[i][j].getDuration().intValue() +13*i+13*j;
            }

        }
        return hash;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Task[][] getData() {
        return data;
    }

    public void setData(Task[][] data) {
        this.data = data;
    }

    public int getM() {
        return M;
    }

    public void setM(int m) {
        M = m;
    }

    public int getN() {
        return N;
    }

    public void setN(int n) {
        N = n;
    }
}


