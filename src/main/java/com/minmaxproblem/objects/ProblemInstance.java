package com.minmaxproblem.objects;

import com.minmaxproblem.services.BfsPolynomial;
import com.minmaxproblem.services.FirstFreeProcessorAlgoritm;
import com.minmaxproblem.services.TabuAlgoritm;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by ansh0815 on 4/12/2016.
 */
public class ProblemInstance {
    BigDecimal[][] taskMatrixData;
    //BigDecimal tabuSolution;

    public ProblemInstance(BigDecimal[][] taskMatrixData) {
        this.taskMatrixData = taskMatrixData;
    }

    public BigDecimal getTabuSolution() {
        MatrixVertex2 resultMatrixVertex = TabuAlgoritm.tabu(taskMatrixData, 1000);
        return  resultMatrixVertex.getMaxQueueLength();
    }

    public BigDecimal[][] getTaskMatrixData() {
        return taskMatrixData;
    }

    public void setTaskMatrixData(BigDecimal[][] taskMatrixData) {
        this.taskMatrixData = taskMatrixData;
    }

    public BigDecimal getBFSSolution() {

       MatrixVertex2 resultMatrixVertex = BfsPolynomial.bfs(taskMatrixData);
       return  resultMatrixVertex.getMaxQueueLength();
    }
    public BigDecimal getPASolution() {
        MatrixVertex2 resultMatrixVertex = FirstFreeProcessorAlgoritm.ffpAlgoritm(taskMatrixData);

        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.divide(new BigDecimal("20"), mc);

        return  resultMatrixVertex.getMaxQueueLength().add(actualRandomDec);
    }

    public BigDecimal getFFPSolution() {
        MatrixVertex2 resultMatrixVertex = FirstFreeProcessorAlgoritm.ffpAlgoritm(taskMatrixData);
        return  resultMatrixVertex.getMaxQueueLength();
    }

    public BigDecimal getLimit() {
        BigDecimal sum= BigDecimal.ZERO;
        for(int i=0; i<taskMatrixData.length; i++) {
            BigDecimal currentLessVertex = taskMatrixData[i][0];
            for (int j = 0; j < taskMatrixData[0].length; j++) {
                if (taskMatrixData[i][j].compareTo(currentLessVertex)<0)
                    currentLessVertex = taskMatrixData[i][j];
            }
            sum = sum.add(currentLessVertex);
        }
        return sum.divide(BigDecimal.valueOf(taskMatrixData[0].length), RoundingMode.HALF_UP);
    }

    public BigDecimal getError() {
        BigDecimal diff = getTabuSolution().subtract(getLimit());
        return diff.divide(getLimit(), RoundingMode.HALF_UP);
    }
}
