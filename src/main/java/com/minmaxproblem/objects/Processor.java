package com.minmaxproblem.objects;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class Processor {
    BigDecimal performance;

    BigDecimal queueDuration = new BigDecimal("0");

    ArrayList<Task> taskToExecute =new ArrayList<Task>();

    public BigDecimal getPerformance() {
        return performance;
    }

    public void setPerformance(BigDecimal performance) {
        this.performance = performance;
    }
    public ArrayList<Task> getTaskToExecute() {
        return taskToExecute;
    }

    public void addTaskToExecute(Task taskToExecute) {
        this.taskToExecute.add(taskToExecute);
        queueDuration = queueDuration.add(taskToExecute.getDuration().multiply(getPerformance()));
    }

    public BigDecimal getQueueDuration() {
        return queueDuration;
    }

    public void setQueueDuration(BigDecimal queueDuration) {
        this.queueDuration = queueDuration;
    }
}
