package com.minmaxproblem.objects;

import java.math.BigDecimal;

public class MatrixVertex2 {


    private int M;             // number of c
    private int N;             // number of r
    private BigDecimal[][] taskMatrix;   // M-by-N array
    private int level;
    public boolean wasVisited;

    public MatrixVertex2(){

    }
    public MatrixVertex2 addVertex(int i, int j, BigDecimal value) {
        this.taskMatrix[i][j]=value;
        level=i;
        return this;
    }
    // create matrix based on 2d array
    /*public MatrixVertex2(MatrixVertex2 matr) {
        N = matr.getData().length;
        M = matr.getData()[0].length;
        this.taskMatrix = new BigDecimal[N][M];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++)
                this.taskMatrix[i][j] = matr.getData()[i][j];
    }*/
    // create matrix based on 2d array
    public MatrixVertex2(BigDecimal[][] data) {
        N = data.length;
        M = data[0].length;
        this.taskMatrix = new BigDecimal[N][M];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++)
                this.taskMatrix[i][j] = data[i][j];
    }

    // create matrix based on 2d array
    public MatrixVertex2(int N, int M) {
        this.N = N;
        this.M = M;
        this.taskMatrix = new BigDecimal[N][M];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++)
                this.taskMatrix[i][j] = new BigDecimal("0");
    }
    // return C = A + B
    public MatrixVertex2 plus(MatrixVertex2 B) {
        MatrixVertex2 A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        MatrixVertex2 C = new MatrixVertex2(N, M);
        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++) {
                C.taskMatrix[i][j]=A.taskMatrix[i][j].add(B.taskMatrix[i][j]);
            }
        return C;
    }

    // return C = A + B
    public MatrixVertex2 addRow(BigDecimal[] taskRow, int row) {
        MatrixVertex2 A = this;
        MatrixVertex2 C = new MatrixVertex2(N, M);
        for (int j = 0; j < M; j++) {
            C.taskMatrix[row][j] = taskRow[j];
        }
        return C;
    }


    // print matrix to standard output
    public void show() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                System.out.print(taskMatrix[i][j]!=null?taskMatrix[i][j]: BigDecimal.ZERO +" ");
            }
            System.out.println();
        }
    }
    public BigDecimal getLength(BigDecimal[] row) {
        BigDecimal sum = new BigDecimal("0");
        for (int j = 0; j < row.length; j++) {
            if (row[j]!=null) {
                sum=sum.add(row[j]);
                //System.out.println( " sum " + sum);
            }

        }

        return sum;
    }
    //getMinMaxQueue
    public BigDecimal getMaxQueueLength() {
        BigDecimal maxLength = BigDecimal.ZERO;
        for (int i = 0; i < N; i++) {
            maxLength = maxLength.add(taskMatrix[i][0]);
        }

        for (int j = 0; j < M; j++) {
            BigDecimal currentLength = BigDecimal.ZERO;
            for (int i = 0; i < N; i++) {
            currentLength = currentLength.add(taskMatrix[i][j]);
            }
            //System.out.println("maxLength "+maxLength);
            //System.out.println("currentLength "+currentLength);
            if (currentLength.compareTo(maxLength)>0) {
               // System.out.println("maxLength "+maxLength);
               /// System.out.println("currentLength "+currentLength);
                maxLength = currentLength;
            }
            System.out.println();
        }

        return maxLength;
    }
    //get MinQueue from MatrixVertex
    public int getMinQueue() {
        BigDecimal minLength = BigDecimal.ZERO;
        int minJ = 0;
        for (int i = 0; i < N; i++) {
            minLength = minLength.add(taskMatrix[i][0]);
        }

        for (int j = 0; j < M; j++) {
            BigDecimal currentLength = BigDecimal.ZERO;
            for (int i = 0; i < N; i++) {
                currentLength = currentLength.add(taskMatrix[i][j]);
            }
            if (currentLength.compareTo(minLength)<0) {
                minLength = currentLength;
                minJ =j;
            }
            System.out.println();
        }

        return minJ;
    }
    //get MaxQueue from MatrixVertex
    public int getMaxQueue() {
        BigDecimal maxLength = BigDecimal.ZERO;
        int maxJ = 0;
        for (int i = 0; i < N; i++) {
            maxLength = maxLength.add(taskMatrix[i][0]);
        }

        for (int j = 0; j < M; j++) {
            BigDecimal currentLength = BigDecimal.ZERO;
            for (int i = 0; i < N; i++) {
                currentLength = currentLength.add(taskMatrix[i][j]);
            }
            if (currentLength.compareTo(maxLength)>0) {
                maxLength = currentLength;
                maxJ =j;
            }
            System.out.println();
        }

        return maxJ;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++){
                if (taskMatrix[i][j]!=null)
                hash = 89 * taskMatrix[i][j].intValue() +13*i+13*j;
            }

        }
        return hash;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public BigDecimal[][] getData() {
        return taskMatrix;
    }

    public void setData(Task[][] data) {
        this.taskMatrix = taskMatrix;
    }

    public int getM() {
        return M;
    }

    public void setM(int m) {
        M = m;
    }

    public int getN() {
        return N;
    }

    public void setN(int n) {
        N = n;
    }
    @Override
    public String toString() {
        String string = " ";
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                string = string.concat(this.taskMatrix[i][j].toString());
                string = string.concat(" ");
            }
            string = string.concat(" \n");
        }
        return string;
    }
}


