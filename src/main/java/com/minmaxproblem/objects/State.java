package com.minmaxproblem.objects;

/**
 * Created by Анастасия on 19.03.2016.
 */
public class State {
    int hashCode;
    int maxQueue;
    int minQueue;

    public State(int hashCode, int maxQueue, int minQueue) {
        this.hashCode = hashCode;
        this.maxQueue = maxQueue;
        this.minQueue = minQueue;
    }

    public int getMaxQueue() {
        return maxQueue;
    }

    public void setMaxQueue(int maxQueue) {
        this.maxQueue = maxQueue;
    }

    public int getHashCode() {
        return hashCode;
    }

    public void setHashCode(int hashCode) {
        this.hashCode = hashCode;
    }

    public int getMinQueue() {
        return minQueue;
    }

    public void setMinQueue(int minQueue) {
        this.minQueue = minQueue;
    }

    @Override
    public boolean equals(Object obj) {
        final State state = (State) obj;
        return this.hashCode == state.getHashCode()
                && this.minQueue == state.getMinQueue()
                && this.maxQueue == state.getMaxQueue();
    }
}
