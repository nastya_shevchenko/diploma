package com.minmaxproblem.objects;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Анастасия on 07.02.2016.
 */
public class Task {
    BigDecimal duration;

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }
}
