package com.minmaxproblem.objects;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by ansh0815 on 4/5/2016.
 */
public class Problem {
    Integer id;
    BigDecimal expectation;
    BigDecimal dispersion;
    BigInteger taskAmount;
    BigInteger procAmount;

    public Problem(){}

    public Problem(int id, BigDecimal expectation, BigDecimal dispersion, BigInteger taskAmount, BigInteger procAmount) {
        this.id = id;
        this.expectation = expectation;
        this.dispersion = dispersion;
        this.taskAmount = taskAmount;
        this.procAmount = procAmount;
    }

    public BigDecimal getExpectation() {
        return expectation;
    }

    public void setExpectation(BigDecimal expectation) {
        this.expectation = expectation;
    }

    public BigDecimal getDispersion() {
        return dispersion;
    }

    public void setDispersion(BigDecimal dispersion) {
        this.dispersion = dispersion;
    }

    public BigInteger getTaskAmount() {
        return taskAmount;
    }

    public void setTaskAmount(BigInteger taskAmount) {
        this.taskAmount = taskAmount;
    }

    public BigInteger getProcAmount() {
        return procAmount;
    }

    public void setProcAmount(BigInteger procAmount) {
        this.procAmount = procAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isNew() {
        return (this.id == null);
    }

}
