package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex;
import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.Processor;
import com.minmaxproblem.objects.Task;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

public class FirstFreeProcessorAlgoritm {

    public static ArrayList<Processor>  startFirstFreeProcessorAlgoritm(ArrayList<Processor> prScope, ArrayList<Task> taskScope){
        ArrayList<Processor> processorScope = prScope;
        final Iterator<Task> it = taskScope.iterator();
        while (it.hasNext()) {
            Task currentTask = it.next();
            Processor pr = findMostFreeProcessor(processorScope);
            pr.addTaskToExecute(currentTask);
        }
        return processorScope;
    }
    public static Processor findMostFreeProcessor(ArrayList<Processor> processorScope) {
        Processor minQueueProc = processorScope.iterator().next();
        for (Processor pr:processorScope) {
            if (pr.getQueueDuration()!=null && pr.getQueueDuration().compareTo(minQueueProc.getQueueDuration())<0){
                minQueueProc = pr;
            }
        }
        return  minQueueProc;
    }

    public static MatrixVertex2 ffpAlgoritm(BigDecimal[][] taskMatrixData) {
        MatrixVertex2 bestMatrixVertex2= new MatrixVertex2(taskMatrixData.length, taskMatrixData[0].length);
        for(int i=0; i<taskMatrixData.length; i++) {
            int j = findLessVertex(taskMatrixData[i]);
            bestMatrixVertex2.addVertex(i, j, taskMatrixData[i][j]);
        }

        return bestMatrixVertex2;

    }
    private static int findLessVertex(BigDecimal[] vector) {
        int lessJ =0;
        BigDecimal lessvalue=vector[0];
        for(int i=0; i<vector.length; i++) {
            if (vector[i].compareTo(lessvalue)<0){
                lessvalue=vector[i];
                lessJ=i;
            }
        }
        return lessJ;

    }

}
