package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex2;

import java.math.BigDecimal;

/**
 * Created by Анастасия on 20.03.2016.
 */
public class StartFirstFreeAlg2 {

    public static MatrixVertex2 startFirstFreeProcessorAlgoritm(BigDecimal[][] taskMatrix) {
        int N = taskMatrix.length;
        int M = taskMatrix[0].length;
        MatrixVertex2 A = new MatrixVertex2(N, M);
        for (int j = 0; j < M; j++) {
            int pr = A.getMinQueue();
            A.getData()[pr][j] = taskMatrix[pr][j];

        }
        return A;
    }
}