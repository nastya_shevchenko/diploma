package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex;
import com.minmaxproblem.objects.Task;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
/**
 * Created by ansh0815 on 3/3/2016.
 */
public class ExcelUtils {
    public static MatrixVertex readSheet() throws IOException {
        int N = 50;
        int M = 390;
        MatrixVertex matrixVertex = new MatrixVertex(N, M);

        try {
            FileInputStream file = new FileInputStream(new File("Welcome.xlsx"));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(2);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();

            Task[][] data = matrixVertex.getData();
            for (int numR = 52; numR < 101; numR++) {
                Row r = sheet.getRow(numR);
                System.out.println(r);
                for (int numC = 0; numC < CellReference.convertColStringToIndex("NZ")-1; numC++) {
                    Cell c = r.getCell(numC);
                    Task task = new Task();
                    int qwe=numR-52;
                    System.out.println(c.getNumericCellValue());
                    task.setDuration(BigDecimal.valueOf(c.getNumericCellValue()));
                    data[numR-52][numC] = task;
                }
            }

            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matrixVertex;
    }
}

