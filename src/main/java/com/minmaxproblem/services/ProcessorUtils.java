package com.minmaxproblem.services;

import com.minmaxproblem.objects.Processor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

public class ProcessorUtils {
    public static ArrayList<Processor> generateProcessorScope(BigDecimal expectation, BigDecimal dispersion, BigInteger processorAmount) {
        ArrayList<Processor> processorList = new ArrayList<Processor>();
        for (BigInteger i = BigInteger.valueOf(0);
             i.compareTo(processorAmount) < 0;
             i = i.add(BigInteger.ONE)){
            Processor processor = new Processor();
            processor.setPerformance(nextRandomBigDecimal(expectation.subtract(dispersion), expectation.add(dispersion)));
            processorList.add(processor);

        }
        return processorList;
    }

    public static BigDecimal nextRandomBigDecimal(BigDecimal min, BigDecimal max) {
        //Random rand = new Random();
        //BigDecimal max = new BigDecimal(range + ".0");
        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.divide(max.subtract(min),mc).add(min);
        return actualRandomDec;
    }

    public static Processor findMaxQueue(ArrayList<Processor> processorList) {
        Processor maxQueueProcessor = processorList.iterator().next();
        for(Processor proc: processorList){
            if (proc.getQueueDuration().compareTo(maxQueueProcessor.getQueueDuration())>=0) {
                maxQueueProcessor = proc;
            }
        }
        return maxQueueProcessor;
    }

    public static Processor findMinQueue(ArrayList<Processor> processorList) {
        Processor minQueueProcessor = processorList.iterator().next();
        for(Processor proc: processorList){
            if (proc.getQueueDuration().compareTo(minQueueProcessor.getQueueDuration())<=0) {
                minQueueProcessor = proc;
            }
        }
        return minQueueProcessor;
    }
}
