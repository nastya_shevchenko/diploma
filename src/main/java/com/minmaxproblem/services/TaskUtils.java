package com.minmaxproblem.services;

import com.minmaxproblem.objects.Problem;
import com.minmaxproblem.objects.Task;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Анастасия on 07.02.2016.
 */
@Service("taskUtils")
public class TaskUtils {

    static ArrayList<Problem> list = new ArrayList<Problem>();

    public static ArrayList<Task> generateTaskScope(BigDecimal expectation, BigDecimal dispersion, BigInteger taskAmount) {
        ArrayList<Task> taskList = new ArrayList<Task>();
        for (BigInteger i = BigInteger.valueOf(0);
                    i.compareTo(taskAmount) < 0;
                    i = i.add(BigInteger.ONE)) {
            Task task = new Task();
            task.setDuration(nextRandomBigDecimal(expectation.subtract(dispersion), expectation.add(dispersion)));
            taskList.add(task);
        }
        return taskList;
    }

    public static BigDecimal nextRandomBigDecimal(BigDecimal min, BigDecimal max) {
        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.divide(max.subtract(min), mc).add(min);
        return actualRandomDec;

    }

    public static BigDecimal[][] generateTaskMatrix(BigDecimal expectation, BigDecimal dispersion, BigInteger taskAmount, BigInteger procAmount) {
        BigDecimal[][] taskPerformMatrix = new BigDecimal[taskAmount.intValue()][procAmount.intValue()];
        for (BigInteger i = BigInteger.valueOf(0);
             i.compareTo(taskAmount) < 0;
             i = i.add(BigInteger.ONE)) {
            for (BigInteger j = BigInteger.valueOf(0);
                 j.compareTo(procAmount) < 0;
                 j = j.add(BigInteger.ONE)) {
                taskPerformMatrix[i.intValue()][j.intValue()]=nextRandomBigDecimal(expectation.subtract(dispersion), expectation.add(dispersion));
            }
        }
       // list.add(new Problem(list.size()+1, expectation, dispersion, taskAmount, procAmount));
        return taskPerformMatrix;
    }

    public ArrayList<Problem>  findAll() {
        return list;
    }
    public ArrayList<Problem> saveOrUpdate(Problem problem) {
        if (problem.getId()==null) {
            problem.setId(list.size());
            list.add(problem);
        } else {
           // Problem  pr = findById(problem.getId());
            for(Problem currentProblem: list) {
                if (currentProblem.getId() == problem.getId()) {
                    list.set(currentProblem.getId(), problem);
                }
            }

        }
        return list;
    }
    public boolean isNew(Problem problem) {
        return list.contains(problem);
    }

    public Problem findById(int i) {
        for (Problem problem: list) {
            if (problem.getId() == i) {
                return problem;
            }
        }
        return null;
    }

    public TaskUtils delete(int i) {

        Iterator<Problem> it = list.iterator();
        while (it.hasNext()) {
            if (it.next().getId() == i) {
                it.remove();
            }
        }
        return this;
    }
}
