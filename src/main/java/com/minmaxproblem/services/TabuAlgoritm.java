package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.State;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created by ansh0815 on 3/17/2016.
 */
@Service("tabuAlgoritm")
public class TabuAlgoritm {
    public static MatrixVertex2 tabu(BigDecimal[][] taskMatrix, int limit) {
        State[] hashRepository = new State[limit];
        MatrixVertex2 currentBestMatrixVertex= FirstFreeProcessorAlgoritm.ffpAlgoritm(taskMatrix);
        BigDecimal B = currentBestMatrixVertex.getMaxQueueLength();
        System.out.println("max Queue by FirstFreeAlg: " + B);

        //make Tabu

        State state = new State(currentBestMatrixVertex.hashCode()
                ,currentBestMatrixVertex.getMaxQueue()
                ,currentBestMatrixVertex.getMinQueue());
        hashRepository[0] = state;

        for(int q=0;q<30;q++) {
            BigDecimal b=currentBestMatrixVertex.getMaxQueueLength();
            //System.out.println("currentBestMatrixVertex before simulation!!!!!!!!" + b);
            currentBestMatrixVertex = doSimulation(currentBestMatrixVertex,
                    currentBestMatrixVertex.getMaxQueue()
                    ,currentBestMatrixVertex.getMinQueue(),
                    taskMatrix);

            //hashcode no in repo
        }
        System.out.print(" currentBestMatrixVertex after simulation :" + currentBestMatrixVertex.getMaxQueueLength());
        return currentBestMatrixVertex;

    }

    private static MatrixVertex2 doSimulation(MatrixVertex2 currentBestMatrixVertex, int maxQueue, int minQueue, BigDecimal[][] taskData) {
       // System.out.println("-------------------------------------------------------------------------------------------------");
        MatrixVertex2 simulationMatrixVertex = new MatrixVertex2(currentBestMatrixVertex.getData());
        BigDecimal simulationBestMaxQueue = simulationMatrixVertex.getMaxQueueLength();
       // System.out.println("Current best queuebefore simulation" + simulationBestMaxQueue);
        Random rn = new Random();
        for(int q=0;q<30;q++) {
            int firstFantomInt = rn.nextInt(simulationMatrixVertex.getData()[maxQueue].length);
            int secondFantomInt = rn.nextInt(simulationMatrixVertex.getData()[minQueue].length);

            simulationMatrixVertex.getData()[maxQueue][firstFantomInt]=BigDecimal.ZERO;
            simulationMatrixVertex.getData()[maxQueue][secondFantomInt]=taskData[maxQueue][secondFantomInt];
            simulationMatrixVertex.getData()[minQueue][secondFantomInt]=BigDecimal.ZERO;
            simulationMatrixVertex.getData()[minQueue][firstFantomInt]=taskData[minQueue][firstFantomInt];

            if (simulationMatrixVertex.getMaxQueueLength().compareTo(currentBestMatrixVertex.getMaxQueueLength())<0) {
                currentBestMatrixVertex = new MatrixVertex2(simulationMatrixVertex.getData());

                System.out.print(" Found better queue " + currentBestMatrixVertex.getMaxQueueLength());

            }

        }
        return currentBestMatrixVertex;
    }

}
