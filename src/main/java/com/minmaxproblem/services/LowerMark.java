package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex2;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by ansh0815 on 4/15/2016.
 */
public class LowerMark {
    public static BigDecimal lowerMark(BigDecimal[][] taskMatrix) {
        BigDecimal T=new BigDecimal(0);

        for (int j = 0; j < taskMatrix[0].length; j++) {
            BigDecimal zn= new BigDecimal(0);
            for (int i = 0; i < taskMatrix.length; i++) {
                zn=zn.add(BigDecimal.ONE.divide(taskMatrix[i][j],4, RoundingMode.HALF_UP));
                //System.out.println("i "+i+" j "+j);
            }
            T=T.add(BigDecimal.ONE.divide(zn,4, RoundingMode.HALF_UP));
        }
        return T;
    }
}
