package com.minmaxproblem.services;

import com.minmaxproblem.objects.MatrixVertex;
import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.Processor;
import com.minmaxproblem.objects.Task;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BfsPolynomial {

    MatrixVertex bestMatrixVertex;
    Queue<MatrixVertex>  taskQueue = new LinkedList<MatrixVertex>();

    public static MatrixVertex2 bfs(BigDecimal[][] taskMatrixData) {

        MatrixVertex2 bestMatrixVertex2=null;
        BigDecimal limit = FirstFreeProcessorAlgoritm.ffpAlgoritm(taskMatrixData).getMaxQueueLength();
        Queue<MatrixVertex2>  taskQueue = new ConcurrentLinkedQueue<>();

        for(int j=0; j<taskMatrixData[0].length; j++) {
            MatrixVertex2 m = new MatrixVertex2(taskMatrixData.length, taskMatrixData[0].length).addVertex(0,j,taskMatrixData[0][j]);
            taskQueue.add(m);
        }
        //for (MatrixVertex2 el:taskQueue) {
        //    System.out.println("t el "+el);
        //}
        Iterator<MatrixVertex2> it = taskQueue.iterator();
        while (it.hasNext()) {
           MatrixVertex2 currentMatrixVertex2 = (MatrixVertex2) it.next();
           int levelToSet =currentMatrixVertex2.getLevel()+1;
           if(currentMatrixVertex2.getMaxQueueLength().compareTo(limit)<0 && currentMatrixVertex2.getLevel() < currentMatrixVertex2.getData().length-1) {
               for(int j=0; j<currentMatrixVertex2.getData()[levelToSet].length; j++) {
                   taskQueue.add(new MatrixVertex2(currentMatrixVertex2.getData())
                           .addVertex(levelToSet,j,taskMatrixData[levelToSet][j]));
               }
           }
           //if last
           if (currentMatrixVertex2.getLevel() == currentMatrixVertex2.getData().length-1) {
               if (bestMatrixVertex2!=null) {
                   if (currentMatrixVertex2.getMaxQueueLength().compareTo(bestMatrixVertex2.getMaxQueueLength())<0) {
                       bestMatrixVertex2 = currentMatrixVertex2;
                       System.out.println("bestMatrixVertex2 "+bestMatrixVertex2);
                   }

               } else {
                  bestMatrixVertex2 = currentMatrixVertex2;
                   System.out.println("bestMatrixVertex2  1111 "+bestMatrixVertex2);
               }

           }
           // for (MatrixVertex2 el:taskQueue) {
           //     System.out.println("taskQueue el "+el);
           // }

           it.remove();
        }
        bestMatrixVertex2.show();
        return bestMatrixVertex2;
    }

    public BigDecimal getMaxQueue(MatrixVertex matrix, ArrayList<Processor> processorList) {

    BigDecimal maxQueue = new BigDecimal("0");
    Task[][] taskMatr = matrix.getData();
         for (int j = 0; j < matrix.getN(); j++) {
             BigDecimal currentMaxQueue = new BigDecimal("0");
             for (int i = 0; i < matrix.getM(); i++) {
                 currentMaxQueue = currentMaxQueue.add(taskMatr[i][j]!=null?
                         taskMatr[i][j].getDuration().multiply(processorList.get(j).getPerformance())
                         :new BigDecimal("0"));
             }

             if (currentMaxQueue.compareTo(maxQueue)>0) {
                 maxQueue=currentMaxQueue;
              }
        }
        return maxQueue;
    }

    public Queue<MatrixVertex> getTaskQueue() {
        return taskQueue;
    }

    public void setTaskQueue(Queue<MatrixVertex> taskQueue) {
        this.taskQueue = taskQueue;
    }
    public MatrixVertex getBestMatrixVertex() {
        return bestMatrixVertex;
    }

    public void setBestMatrixVertex(MatrixVertex bestMatrixVertex) {
        this.bestMatrixVertex = bestMatrixVertex;
    }


    public void printQueue(Queue<MatrixVertex>  taskQueue, int I, int J ) {
        System.out.println(" --------------------------------------------------");
        for (MatrixVertex ver :taskQueue) {
            for (int i = 0; i < I; i++) {
                for (int j = 0; j < J; j++) {

                    System.out.print("___ " + ver!=null && ver.getData()[i][j] !=null?ver.getData()[i][j].getDuration(): null);
                }
                System.out.println(" ");
            }
            System.out.println(" ");
        }
    }
}
