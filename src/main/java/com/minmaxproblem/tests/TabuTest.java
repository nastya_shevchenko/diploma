package com.minmaxproblem.tests;

import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.Processor;
import com.minmaxproblem.objects.Task;
import com.minmaxproblem.services.LowerMark;
import com.minmaxproblem.services.ProcessorUtils;
import com.minmaxproblem.services.TabuAlgoritm;
import com.minmaxproblem.services.TaskUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by ansh0815 on 3/3/2016.
 */
public class TabuTest {

    @Test
    public void testTabu(/*int limit*/) {

       /* ArrayList<Task> taskList = TaskUtils.generateTaskScope(
                new BigDecimal(1000000.0),
                new BigDecimal(1000.0),
                new BigInteger("10"));

        for (Task task : taskList)
            System.out.println("tasks: " + task.getDuration());

        ArrayList<Processor> processorList = ProcessorUtils.generateProcessorScope(
                BigDecimal.valueOf(3.0),
                BigDecimal.valueOf(3.0),
                BigInteger.valueOf(3));*/

        BigDecimal[][] taskMatrixData = TaskUtils.generateTaskMatrix(BigDecimal.valueOf(100.0), BigDecimal.valueOf(10.0),
                new BigInteger("50"), new BigInteger("3"));
        MatrixVertex2 resultMatrixVertex = TabuAlgoritm.tabu(taskMatrixData, 1000);
        //BigDecimal T = LowerMark.lowerMark(taskMatrixData);
        System.out.println("T " + resultMatrixVertex.getMaxQueueLength());

    }
}
