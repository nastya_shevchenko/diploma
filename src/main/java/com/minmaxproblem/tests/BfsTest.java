package com.minmaxproblem.tests;

import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.ProblemInstance;
import com.minmaxproblem.objects.Processor;
import com.minmaxproblem.objects.Task;
import com.minmaxproblem.services.BfsPolynomial;
import com.minmaxproblem.services.FirstFreeProcessorAlgoritm;
import com.minmaxproblem.services.ProcessorUtils;
import com.minmaxproblem.services.TaskUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Анастасия on 09.02.2016.
 */
public class BfsTest {
    @Test
    public void testBfs(){
       /* ArrayList<Task> taskList = TaskUtils.generateTaskScope(
                new BigDecimal(1000000.0),
                new BigDecimal(1000.0),
                new BigInteger("10"));
       // ArrayList<Task> taskList2 = new

        for(Task task:taskList)
            System.out.println("tasks: " + task.getDuration());

        ArrayList<Processor> processorList = ProcessorUtils.generateProcessorScope(
                new BigDecimal(3.0),
                new BigDecimal(2.0),
                new BigInteger("3"));


        processorList = FirstFreeProcessorAlgoritm.startFirstFreeProcessorAlgoritm(processorList, taskList);

        BigDecimal B = ProcessorUtils.findMaxQueue(processorList).getQueueDuration();
        System.out.println("max Queue by FirstFreeAlg: " +B);

        BfsPolynomial bfs = new BfsPolynomial();

        bfs.bfs(processorList, taskList, B);
            System.out.println(" bfs best plan ");
            bfs.getBestMatrixVertex().show();
        System.out.println("max Queue by bfs: " +bfs.getMaxQueue(bfs.getBestMatrixVertex(),processorList));*/
        //MatrixVertex2 bfsMatrix = BfsPolynomial.bfs(TaskUtils.generateTaskMatrix(BigDecimal.valueOf(100.0),
          //      BigDecimal.valueOf(10.0),BigInteger.valueOf(6),BigInteger.valueOf(3)));
        //System.out.println("Bfs: " + bfsMatrix.getMaxQueueLength());

        BigDecimal[][] bfsMatrix = TaskUtils.generateTaskMatrix(BigDecimal.valueOf(100.0),
                BigDecimal.valueOf(10.0), BigInteger.valueOf(6), BigInteger.valueOf(3));

        ProblemInstance pr = new ProblemInstance(bfsMatrix);
        System.out.print(pr.getLimit());
    }
}
