package com.minmaxproblem.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.minmaxproblem.objects.MatrixVertex2;
import com.minmaxproblem.objects.Problem;
import com.minmaxproblem.objects.ProblemInstance;
import com.minmaxproblem.services.TabuAlgoritm;
import com.minmaxproblem.services.TaskUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by ansh0815 on 3/30/2016.
 */
@Controller
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private TaskUtils taskUtils;

    @Autowired
    private TabuAlgoritm tabuAlgoritm;

    //@Autowired
    //UserFormValidator userFormValidator;

    //Set a form validator
    //@InitBinder
    // protected void initBinder(WebDataBinder binder) {
   //     binder.setValidator(userFormValidator);
   // }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        logger.debug("index()");
        return "redirect:/list";
    }

    // list page
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showAllProblems(Model model) {

        logger.debug("showAllProblems()");
        model.addAttribute("list", taskUtils.findAll());
        return "ui/list";

    }

    // save or update user
    // 1. @ModelAttribute bind form value
    // 2. @Validated form validator
    // 3. RedirectAttributes for flash value
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public String saveOrUpdateproblem(@ModelAttribute("problemForm") Problem problem,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateUser() : {}", problem);

        if (result.hasErrors()) {
           // populateDefaultModel(model);
            return "ui/problemform";
        } else {

            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(taskUtils.isNew(problem)) {
                redirectAttributes.addFlashAttribute("msg", "Problem added successfully!");
            } else {
                redirectAttributes.addFlashAttribute("msg", "Problem updated successfully!");
            }
            System.out.println("id  " + problem.getId());
            taskUtils.saveOrUpdate(problem);

            // POST/REDIRECT/GET
            return "redirect:/list/" + problem.getId();

        }

    }

    // show add user form
    @RequestMapping(value = "/list/add", method = RequestMethod.GET)
    public String showAddProblemForm(Model model) {

        logger.debug("showAddProblemForm()");

        Problem problem = new Problem();

        // set default value
        problem.setDispersion(new BigDecimal(2));
        problem.setExpectation(new BigDecimal(100));
        problem.setProcAmount(new BigInteger("2"));
        problem.setTaskAmount(new BigInteger("50"));

        model.addAttribute("problemForm", problem);

       // populateDefaultModel(model);

        return "ui/problemfrom";

    }

    // show update form
    @RequestMapping(value = "/list/{id}/update", method = RequestMethod.GET)
    public String showUpdateProblemForm(@PathVariable("id") int id, Model model) {

        logger.debug("showUpdateUserForm() : {}", id);

        Problem problem = taskUtils.findById(id);
        model.addAttribute("problemForm", problem);

        taskUtils.saveOrUpdate(problem);
       // populateDefaultModel(model);

        return "ui/problemfrom";

    }

    // delete user
    @RequestMapping(value = "/list/{id}/delete", method = RequestMethod.POST)
    public String deleteProblem(@PathVariable("id") int id,
                             final RedirectAttributes redirectAttributes) {

        logger.debug("deleteProblem() : {}", id);

        taskUtils.delete(id);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Problem is deleted!");

        return "redirect:/list";

    }
    // solution
    @RequestMapping(value = "/list/{id}/solution", method = RequestMethod.GET)
    public String solution(@PathVariable("id") int id, Model model) {
        Problem problem = taskUtils.findById(id);
        ArrayList<ProblemInstance> problemInstances = new ArrayList<ProblemInstance>();

        for (int i=0; i<10;i++) {
            problemInstances.add(new ProblemInstance(taskUtils.generateTaskMatrix(problem.getExpectation(),
                    problem.getDispersion(),
                    problem.getTaskAmount(),
                    problem.getProcAmount())));
        }
        model.addAttribute("problemInstances", problemInstances);

        return "ui/solution";
    }

    // show user
    @RequestMapping(value = "/list/{id}", method = RequestMethod.GET)
    public String showProblem(@PathVariable("id") int id, Model model) {

        logger.debug("showProblem() id: {}", id);

        Problem problem = taskUtils.findById(id);
        if (problem == null) {
            model.addAttribute("css", "danger");
            model.addAttribute("msg", "Problem not found");
        }
        model.addAttribute("problem", problem);

        return "ui/show";

    }

    /*private void populateDefaultModel(Model model) {

        List<String> frameworksList = new ArrayList<String>();
        frameworksList.add("Spring MVC");
        frameworksList.add("Struts 2");
        frameworksList.add("JSF 2");
        frameworksList.add("GWT");
        frameworksList.add("Play");
        frameworksList.add("Apache Wicket");
        model.addAttribute("frameworkList", frameworksList);

        Map<String, String> skill = new LinkedHashMap<String, String>();
        skill.put("Hibernate", "Hibernate");
        skill.put("Spring", "Spring");
        skill.put("Struts", "Struts");
        skill.put("Groovy", "Groovy");
        skill.put("Grails", "Grails");
        model.addAttribute("javaSkillList", skill);

        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        model.addAttribute("numberList", numbers);

        Map<String, String> country = new LinkedHashMap<String, String>();
        country.put("US", "United Stated");
        country.put("CN", "China");
        country.put("SG", "Singapore");
        country.put("MY", "Malaysia");
        model.addAttribute("countryList", country);

    }*/

}
//http://www.mkyong.com/spring-mvc/spring-mvc-form-handling-example/