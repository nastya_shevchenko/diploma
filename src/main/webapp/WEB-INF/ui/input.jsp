<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Minimax scheduling task Algorithms</title>
</head>
<body>

<h2>Input please tasks properties</h2>
<form:form method="POST" action="/addInput">
   <table>
    <tr>
        <td><form:label path="N">N</form:label></td>
        <td><form:input path="N" /></td>
    </tr>
    <tr>
        <td><form:label path="M">M</form:label></td>
        <td><form:input path="M" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
</body>
</html>