<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1>Problem Simulation</h1>
    <br />
    <table class="table table-striped">
        <thead>
        <tr>
            <th>First Free Processor Algoritm</th>
            <th>Probabilistic algoritm</th>
            <th>BFS Polinomial Algoritm</th>
            <th>Tabu algoritm</th>
            <th>Lower Line</th>
            <th>Error</th>
        </tr>
        </thead>

        <c:forEach var="problemInstance" items="${problemInstances}">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>${problemInstance.tabuSolution}</td>
                <td></td>
                <td></td>

            </tr>
        </c:forEach>
    </table>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>