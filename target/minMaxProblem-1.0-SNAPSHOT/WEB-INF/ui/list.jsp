<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>

<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1>All Problems</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#ID</th>
            <th>expectation</th>
            <th>dispersion</th>
            <th>taskAmount</th>
            <th>procAmount</th>
        </tr>
        </thead>

        <c:forEach var="problem" items="${list}">
            <tr>
                <td>${problem.id}</td>
                <td>${problem.expectation}</td>
                <td>${problem.dispersion}</td>
                <td>${problem.taskAmount}</td>
                <td>${problem.procAmount}</td>
                <td>
                    <c:url value="/list/${problem.id}/solution" var="userUrl" />
                    <c:url value="/list/${problem.id}/delete" var="deleteUrl" />
                    <c:url value="/list/${problem.id}/update" var="updateUrl" />

                    <button class="btn btn-info"
                            onclick="location.href='${userUrl}'">Solution</button>
                    <button class="btn btn-primary"
                            onclick="location.href='${updateUrl}'">Update</button>
                    <button class="btn btn-danger"
                            onclick="this.disabled=true;post('${deleteUrl}')">Delete</button>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>