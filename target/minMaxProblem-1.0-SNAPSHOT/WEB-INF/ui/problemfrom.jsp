<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">

    <c:choose>
         <c:when test="${problemForm['new']}">
             <h1>Add Problem</h1>
         </c:when>
         <c:otherwise>
             <h1>Update Problem</h1>
         </c:otherwise>
     </c:choose>
    <br />
    <th>#ID</th>
    <th>expectation</th>
    <th>dispersion</th>
    <th>taskAmount</th>
    <th>procAmount</th>

    <spring:url value="/list" var="listActionUrl" />

        <form:form class="form-horizontal" method="post"
                   modelAttribute="problemForm" action="${listActionUrl}">

                <form:hidden path="id" />
                <spring:bind path="expectation">

                    <label class="col-sm-2 control-label">Expectation</label>
                    <div class="col-sm-10">
                        <form:input path="expectation" type="text" class="form-control"
                                    id="expectation" placeholder="expectation" />
                        <form:errors path="expectation" class="control-label" />
                    </div>

                </spring:bind>

                <spring:bind path="dispersion">

                    <label class="col-sm-2 control-label">Dispersion</label>
                    <div class="col-sm-10">
                        <form:input path="dispersion" class="form-control"
                                    id="dispersion" placeholder="dispersion" />
                        <form:errors path="dispersion" class="control-label" />
                    </div>

                </spring:bind>

                <spring:bind path="taskAmount">

                    <label class="col-sm-2 control-label">TaskAmount</label>
                    <div class="col-sm-10">
                        <form:input path="taskAmount" class="form-control"
                                       id="taskAmount" placeholder="taskAmount" />
                        <form:errors path="taskAmount" class="control-label" />
                    </div>

                </spring:bind>

                <spring:bind path="procAmount">
                <label class="col-sm-2 control-label">ProcAmount </label>
                <div class="col-sm-10">
                    <form:input path="procAmount" class="form-control"
                                   id="procAmount" placeholder="procAmount" />
                    <form:errors path="procAmount" class="control-label" />
                </div>
            </div>
            </spring:bind>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <c:choose>
                         <c:when test="${problemForm['new']}">
                            <button type="submit" class="btn-lg btn-primary pull-right">Add
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button type="submit" class="btn-lg btn-primary pull-right">Update
                            </button>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
    </form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>